package gochannels

func Fibs(ch chan int) {
	i, j := 0, 1
	for {
		ch <- i
		i, j = j, i+j
	}
}
